<?php

namespace ApartmentBundle\Controller;

use ApartmentBundle\Entity\Apartment;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Apartment controller.
 *
 * @Route("apartment")
 */
class ApartmentController extends Controller
{
    /**
     * Lists all apartment entities.
     *
     * @Route("/", name="apartment_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $apartments = $em->getRepository('ApartmentBundle:Apartment')->findAll();

        return $this->render('apartment/index.html.twig', array(
            'apartments' => $apartments,
        ));
    }

    /**
     * Creates a new apartment entity.
     *
     * @Route("/new", name="apartment_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $apartment = new Apartment();
        $form = $this->createForm('ApartmentBundle\Form\ApartmentType', $apartment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($apartment);
            $em->flush();

            return $this->redirectToRoute('apartment_show', array('id' => $apartment->getId()));
        }

        return $this->render('apartment/new.html.twig', array(
            'apartment' => $apartment,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a apartment entity.
     *
     * @Route("/{id}", name="apartment_show")
     * @Method("GET")
     */
    public function showAction(Apartment $apartment)
    {
        $deleteForm = $this->createDeleteForm($apartment);

        return $this->render('apartment/show.html.twig', array(
            'apartment' => $apartment,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing apartment entity.
     *
     * @Route("/{id}/edit", name="apartment_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Apartment $apartment)
    {
        $deleteForm = $this->createDeleteForm($apartment);
        $editForm = $this->createForm('ApartmentBundle\Form\ApartmentType', $apartment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('apartment_edit', array('id' => $apartment->getId()));
        }

        return $this->render('apartment/edit.html.twig', array(
            'apartment' => $apartment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a apartment entity.
     *
     * @Route("/{id}", name="apartment_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Apartment $apartment)
    {
        $form = $this->createDeleteForm($apartment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($apartment);
            $em->flush();
        }

        return $this->redirectToRoute('apartment_index');
    }

    /**
     * Creates a form to delete a apartment entity.
     *
     * @param Apartment $apartment The apartment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Apartment $apartment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('apartment_delete', array('id' => $apartment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}

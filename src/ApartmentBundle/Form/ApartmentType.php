<?php

namespace ApartmentBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\DataTransformer\NumberToLocalizedStringTransformer;

class ApartmentType extends AbstractType {

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder->add('price', NumberType::class, [
                    'label' => 'Цена',
                    'rounding_mode' => NumberToLocalizedStringTransformer::ROUND_FLOOR,
                    'required' => true
                ])
                ->add('description', TextareaType::class, [
                    'label' => 'Описание',
                    "attr" => ["rows" => 10]
                ])
                ->add('address', TextType::class, [
                    'label' => 'Адрес',
                    'required' => true
                ])
                ->add('height', EntityType::class, [
                    'label' => 'Высота потолков',
                    'class' => 'ApartmentBundle:HeightVariant',
                    'choice_label' => 'value',
                    'required' => true
                ])
                ->add('space', NumberType::class, [
                    'label' => 'Площадь',
                    'rounding_mode' => NumberToLocalizedStringTransformer::ROUND_FLOOR,
                    'required' => true
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => 'ApartmentBundle\Entity\Apartment'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix() {
        return 'apartmentbundle_apartment';
    }

}

<?php

namespace ApartmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Variants
 *
 * @ORM\Table(name="height_variant")
 * @ORM\Entity
 */
class HeightVariant {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="float", precision=10, scale=0, nullable=true)
     */
    private $value;

    public function getValue() {
        return $this->value;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $value
     *
     * @return Variants
     */
    public function setValue($value) {
        $this->value = $value;

        return $this;
    }

}

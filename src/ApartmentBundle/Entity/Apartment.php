<?php

namespace ApartmentBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use ApartmentBundle\Entity\HeightVariant;

/**
 * Apartment
 *
 * @ORM\Table(name="apartment")
 * @ORM\Entity
 */
class Apartment {

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=10, scale=0, nullable=true)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", length=65535, nullable=true)
     */
    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=70, nullable=true)
     */
    private $address;

    /**
     * @ORM\ManyToOne(targetEntity="HeightVariant", inversedBy="value")
     * @ORM\JoinColumn(name="height", referencedColumnName="id")
     */
    private $height;

    /**
     * @var float
     *
     * @ORM\Column(name="space", type="float", precision=10, scale=0, nullable=true)
     */
    private $space;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Apartment
     */
    public function setPrice($price) {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return Apartment
     */
    public function setDescription($description) {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Apartment
     */
    public function setAddress($address) {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     * Set space
     *
     * @param float $space
     *
     * @return Apartment
     */
    public function setSpace($space) {
        $this->space = $space;

        return $this;
    }

    /**
     * Get space
     *
     * @return float
     */
    public function getSpace() {
        return $this->space;
    }

    /**
     * Set height
     *
     * @param \ApartmentBundle\Entity\HeightVariant $height
     *
     * @return Apartment
     */
    public function setHeight(HeightVariant $height = null) {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return \ApartmentBundle\Entity\HeightVariant
     */
    public function getHeight() {
        return $this->height;
    }

}
